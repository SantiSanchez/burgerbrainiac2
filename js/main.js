var currentRecipe = [];
var currentRecipeAdditions = [];

var currentOrder = [];
var currentOrderAdditions = [];

var orderIngredients = ["tomato", "lettuce", "meat",
    "mustard", "ketchup", "cheese"];

var orderAdditions = ["soda", "fries"];

var additionCoordinatesX = 450;
var additionCoordinatesY = 370;
var burgerCoordinatesX = 670;
var burgerCoordinatesY = 450;

var imageNumber = 3;

var tutorialTimer;
var countTutorialStep = 0;

var timeLeft = 30;

var gameObject = {};

gameObject.params = {
    baseWidth: 1600,
    baseHeight: 900
};

gameObject.Boot = function (game) { };

gameObject.Boot = {
    init: function () {
        this.scale.scaleMode = Phaser.ScaleManager.RESIZE;
    },
    preload: function () {
        /*Start Scene*/
        this.load.image('startBackground', 'assets/img/backgrounds/startScene.png');
        this.load.image('btnPlaynow', 'assets/img/ui/playnow.png');
        this.load.image('btnCredits', 'assets/img/ui/credits.png');
        this.load.image('btnRecords', 'assets/img/ui/records.png');

        /* Records */
        this.load.image('recordsBackground', 'assets/img/backgrounds/recordsBackground.png');
        this.load.image('btnBack', 'assets/img/ui/backArrow.png');

        /* Credits */
        this.load.image('creditsBackground', 'assets/img/backgrounds/creditsBackground.png');

        /* GamePlay */

        /*Scene Sprites*/
        this.load.image('gameBackground', 'assets/img/backgrounds/gameBackground.png');
        this.load.image('ingredientsPanel', 'assets/img/ui/ingredients.png');
        this.load.image('orderPanel', 'assets/img/ui/order.png');
        this.load.image('clock', 'assets/img/ui/clock.png');
        this.load.image('scorePanel', 'assets/img/ui/clock.png');
        /*Tutorial UI Sprites*/
        this.load.image('bubbleOrder', 'assets/img/ui/bubbleorder.png');
        this.load.image('bubbleIngredients', 'assets/img/ui/bubbleingredients.png');
        this.load.image('bubbleClock', 'assets/img/ui/bubbleclock.png');
        this.load.image('btnSkip', 'assets/img/ui/skip.png');
        /*Ingredients Sprites*/
        this.load.image('topBread', '../assets/img/ingredients/topBread.png');
        this.load.image('bottomBread', '../assets/img/ingredients/bottomBread.png');
        this.load.image('cheese', '../assets/img/ingredients/cheese.png');
        this.load.image('fries', '../assets/img/ingredients/fries.png');
        this.load.image('ketchup', '../assets/img/ingredients/ketchup.png');
        this.load.image('lettuce', '../assets/img/ingredients/lettuce.png');
        this.load.image('meat', '../assets/img/ingredients/meat.png');
        this.load.image('mustard', '../assets/img/ingredients/mustard.png');
        this.load.image('soda', '../assets/img/ingredients/soda.png');
        this.load.image('tomato', '../assets/img/ingredients/tomato.png');
        /*Scene UI Sprites*/
        this.load.image('wellDone', 'assets/img/ui/wellDone.png');
        this.load.image('btnNext', 'assets/img/ui/next.png');

        this.load.image('gameover', 'assets/img/ui/gameover.png');
        this.load.image('score', 'assets/img/ui/score.png');
        this.load.image('best', 'assets/img/ui/best.png');
        this.load.image('playagain', 'assets/img/ui/playagain.png');

        this.load.image('3', 'assets/img/ui/3.png');
        this.load.image('2', 'assets/img/ui/2.png');
        this.load.image('1', 'assets/img/ui/1.png');
        /* GamePlay */
    },
    create: function () {
        this.state.start("GameStart");
    }
};
/*Start screen */
gameObject.GameStart = function (game) { };

gameObject.GameStart.prototype = {

    create: function () {
        this.startBackground = this.game.add.image(0, 0, 'startBackground');
        this.startBackground.height = this.game.height;
        this.startBackground.width = this.game.width;

        this.btnPlaynow = this.game.add.button(this.world.centerX, this.world.centerY + this.game.height / 5, 'btnPlaynow', goToGamePlay, this);
        this.btnPlaynow.anchor.setTo(0.5, 0.5);
        this.scaleSprite(this.btnPlaynow, this.game.width, this.game.height);

        this.btnCredits = this.game.add.button(this.game.width - this.game.width / 2.6, this.world.centerY + this.game.height / 3, 'btnCredits', goToCredits, this);
        this.scaleSprite(this.btnCredits, this.game.width, this.game.height);
        this.btnCredits.anchor.setTo(0.5, 0.5);


        this.btnRecords = this.game.add.button(this.game.width / 2.6, this.world.centerY + this.game.height / 3, 'btnRecords', goToRecords, this);
        this.scaleSprite(this.btnRecords, this.game.width, this.game.height);
        this.btnRecords.anchor.setTo(0.5, 0.5);


    },
    /* function to scale the buttons sprites */
    scaleSprite: function (sprite, availableSpaceWidth, availableSpaceHeight) {
        sprite.width = sprite.width / (gameObject.params.baseWidth / availableSpaceWidth);
        sprite.height = sprite.height / (gameObject.params.baseHeight / availableSpaceHeight);
    }
};

/* Records Screen */
gameObject.Records = function (game) { };

gameObject.Records.prototype = {
    create: function () {
        this.recordsBackground = this.game.add.image(0, 0, 'recordsBackground');
        this.recordsBackground.height = this.game.height;
        this.recordsBackground.width = this.game.width;

        this.btnBack = this.game.add.button(80, 50, 'btnBack', goToGameStart, this);
        this.btnBack.anchor.setTo(0.5, 0.5);
        this.scaleText(this.btnBack, this.world.width, this.world.height);
        this.style = { font: "32px Arial", fill: "#ffffff", align: "center" };
        this.textRank1 = this.add.text(this.world.centerX + this.game.width / 16, this.world.centerY + this.game.height / 10, Persistence.load("Rank1") + " Burgers", this.style);
        this.textRank1.anchor.setTo(0.5, 0.5);
        this.scaleText(this.textRank1, this.world.width, this.world.height);
        this.textRank2 = this.add.text(this.world.centerX + this.game.width / 16, this.world.centerY + this.game.height / 4.3, Persistence.load("Rank2") + " Burgers", this.style);
        this.textRank2.anchor.setTo(0.5, 0.5);
        this.scaleText(this.textRank2, this.world.width, this.world.height);
        this.textRank3 = this.add.text(this.world.centerX + this.game.width / 16, this.world.centerY + this.game.height / 2.7, Persistence.load("Rank3") + " Burgers", this.style);
        this.textRank3.anchor.setTo(0.5, 0.5);
        this.scaleText(this.textRank3, this.world.width, this.world.height);
    },
    scaleText: function (sprite, availableSpaceWidth, availableSpaceHeight) {
        sprite.width = (sprite.width / (gameObject.params.baseWidth / availableSpaceWidth)) + 10;
        sprite.height = (sprite.height / (gameObject.params.baseHeight / availableSpaceHeight)) + 10;
    }
};
/* Credits Screen */
gameObject.Credits = function (game) { };

gameObject.Credits.prototype = {
    create: function () {
        this.creditBackground = this.game.add.image(0, 0, 'creditsBackground');
        this.creditBackground.height = this.game.height;
        this.creditBackground.width = this.game.width;

        this.btnBack = this.game.add.button(80, 50, 'btnBack', goToGameStart, this);
        this.btnBack.anchor.setTo(0.5, 0.5);
        this.scaleSprite(this.btnBack, this.game.width, this.game.height);

    },
    /*Function to scale back button*/
    scaleSprite: function (sprite, availableSpaceWidth, availableSpaceHeight) {
        sprite.width = sprite.width / (gameObject.params.baseWidth / availableSpaceWidth);
        sprite.height = sprite.height / (gameObject.params.baseHeight / availableSpaceHeight);
    }

}

/* Gameplay Screen */
gameObject.GamePlay = function (game) { };

gameObject.GamePlay.prototype = {

    create: function () {

        this.burgerCount = 0;
        this.gameBackground = this.add.image(this.world.centerX, this.world.centerY, 'gameBackground');
        this.gameBackground.anchor.setTo(0.5, 0.5);
        this.gameBackground.height = this.game.height;
        this.gameBackground.width = this.game.width;

        this.ingredientsPanel = this.add.sprite(this.world.width, this.world.centerY, 'ingredientsPanel');
        this.ingredientsPanel.anchor.setTo(1, 0.5);
        this.scaleUI(this.ingredientsPanel, this.game.width, this.game.height);

        this.orderPanel = this.add.sprite(0, this.world.centerY, 'orderPanel');
        this.orderPanel.anchor.setTo(0, 0.5);
        this.scaleUI(this.orderPanel, this.game.width, this.game.height);

        this.clock = this.add.sprite(0, 0, 'clock');
        this.clock.anchor.setTo(0, 0);
        this.scaleUI(this.clock, this.game.width, this.game.height);

        this.scorePanel = this.add.sprite(this.world.width, 0, 'scorePanel');
        this.scorePanel.anchor.setTo(1, 0);
        this.scaleUI(this.scorePanel, this.game.width, this.game.height);

        this.style = { font: "32px Catamaran", fill: "#ffffff", wordWrap: true, wordWrapWidth: this.scorePanel.width, align: "center" };

        this.textScore = this.add.text(this, this.game.world.width, 0, this.style);
        this.textScore.anchor.setTo(0.5, 0.5);
        this.scaleUI(this.textScore, this.game.width, this.game.height);
        this.textScore.text = "Burgers:0";

        this.textClock = this.add.text(this, 0, 0, this.style);
        this.textClock.anchor.setTo(0.5, 0.5);
        this.scaleUI(this.textClock, this.game.width, this.game.height);

        this.clockTimer = this.time.create(false);
        this.clockTimer.loop(1000, this.showTimeLeft, this);

        /*Tutorial*/
        tutorialTimer = this.time.create(false);
        tutorialTimer.loop(3000, this.showTutorial, this);
        tutorialTimer.start();
        this.btnSkip = this.game.add.button(this.game.width, this.game.height, 'btnSkip', this.skipTutorial, this);
        this.scaleUI(this.btnSkip, this.game.width, this.game.height);
        this.btnSkip.anchor.setTo(1, 1);
        this.btnSkip.visible = false;

        this.bubbleOrder = this.add.sprite(this.game.width / 9, this.world.centerY, 'bubbleOrder');
        this.scaleUI(this.bubbleOrder, this.game.width, this.game.height);
        this.bubbleOrder.visible = false;

        this.bubbleIngredients = this.add.sprite(this.game.width - 170, this.game.height / 2, 'bubbleIngredients');
        this.scaleUI(this.bubbleIngredients, this.game.width, this.game.height);
        this.bubbleIngredients.anchor.setTo(1, 1);
        this.bubbleIngredients.visible = false;

        this.bubbleClock = this.add.sprite(100, 0, 'bubbleClock');
        this.scaleUI(this.bubbleClock, this.game.width, this.game.height);
        this.bubbleClock.anchor.setTo(0, 0);
        this.bubbleClock.visible = false;
        /*Tutorial*/

        /*Ingredients*/
        this.topBread = this.game.add.button(this.game.width - this.game.width / 13, this.world.centerY - this.game.height / 6, 'topBread', this.addIngredient, this);
        this.topBread.name = "topBread";
        this.topBread.spriteHeight = 25;
        this.topBread.anchor.setTo(0.5, 0.5);
        this.scaleSprite(this.topBread, this.game.width, this.game.height);

        this.tomato = this.game.add.button(this.game.width - this.game.width / 13, this.world.centerY - this.game.height / 12, 'tomato', this.addIngredient, this);
        this.tomato.name = "tomato";
        this.tomato.spriteHeight = 15;
        this.tomato.anchor.setTo(0.5, 0.5);
        this.scaleSprite(this.tomato, this.game.width, this.game.height);

        this.lettuce = this.game.add.button(this.game.width - this.game.width / 13, this.world.centerY, 'lettuce', this.addIngredient, this);
        this.lettuce.name = "lettuce";
        this.lettuce.spriteHeight = 10;
        this.lettuce.anchor.setTo(0.5, 0.5);
        this.scaleSprite(this.lettuce, this.game.width, this.game.height);

        this.meat = this.game.add.button(this.world.width - this.game.width / 13, this.world.centerY + this.game.height / 12, 'meat', this.addIngredient, this);
        this.meat.name = "meat";
        this.meat.spriteHeight = 25;
        this.meat.anchor.setTo(0.5, 0.5);
        this.scaleSprite(this.meat, this.game.width, this.game.height);

        this.bottomBread = this.game.add.button(this.world.width - this.game.width / 13, this.world.centerY + this.game.height / 6, 'bottomBread', this.addIngredient, this);
        this.bottomBread.name = "bottomBread";
        this.bottomBread.spriteHeight = 25;
        this.bottomBread.anchor.setTo(0.5, 0.5);
        this.scaleSprite(this.bottomBread, this.game.width, this.game.height);

        this.mustard = this.game.add.button(this.world.width - this.game.width / 6, this.world.centerY - this.game.height / 6, 'mustard', this.addIngredient, this, 0, 0, 0, 0);
        this.mustard.name = "mustard";
        this.mustard.spriteHeight = 0;
        this.mustard.anchor.setTo(0.5, 0.5);
        this.scaleSprite(this.mustard, this.game.width, this.game.height);

        this.ketchup = this.game.add.button(this.world.width - this.game.width / 6, this.world.centerY - this.game.height / 12, 'ketchup', this.addIngredient, this);
        this.ketchup.name = "ketchup";
        this.ketchup.spriteHeight = 0;
        this.ketchup.anchor.setTo(0.5, 0.5);
        this.scaleSprite(this.ketchup, this.game.width, this.game.height);

        this.cheese = this.game.add.button(this.world.width - this.game.width / 6, this.world.centerY, 'cheese', this.addIngredient, this);
        this.cheese.name = "cheese";
        this.cheese.spriteHeight = 2;
        this.cheese.anchor.setTo(0.5, 0.5);
        this.scaleSprite(this.cheese, this.game.width, this.game.height);

        this.fries = this.game.add.button(this.world.width - this.game.width / 6, this.world.centerY + this.game.height / 12, 'fries', this.addFriesSoda, this);
        this.fries.name = "fries";
        this.fries.anchor.setTo(0.5, 0.5);
        this.scaleAdditions(this.fries, this.game.width, this.game.height);

        this.soda = this.game.add.button(this.world.width - this.game.width / 6, this.world.centerY + this.game.height / 6, 'soda', this.addFriesSoda, this);
        this.soda.name = "soda";
        this.soda.anchor.setTo(0.5, 0.5);
        this.scaleAdditions(this.soda, this.game.width, this.game.height);
        /*Ingredients*/



        this.orderGroup = this.game.add.group(this, "orderGroup", true);
        this.orderGroup.scale.x = 1 / ((gameObject.params.baseWidth / this.game.width) * 2);
        this.orderGroup.scale.y = 1 / ((gameObject.params.baseHeight / this.game.height) * 2);

        this.additionOrderGroup = this.game.add.group(this, "additionOrderGroup", true);
        this.additionOrderGroup.scale.x = 1 / ((gameObject.params.baseWidth / this.game.width) * 2);
        this.additionOrderGroup.scale.y = 1 / ((gameObject.params.baseHeight / this.game.height) * 2);

        this.recipeGroup = this.game.add.group(this, "recipeGroup", true);
        this.recipeGroup.scale.x = 1 / (gameObject.params.baseWidth / this.game.width);
        this.recipeGroup.scale.y = 1 / (gameObject.params.baseHeight / this.game.height);

        this.recipeAdditionGroup = this.game.add.group(this, "recipeAdditionGroup", true);
        this.recipeAdditionGroup.scale.x = 1 / (gameObject.params.baseWidth / this.game.width);
        this.recipeAdditionGroup.scale.y = 1 / (gameObject.params.baseHeight / this.game.height);

        this.winGroup = this.game.add.group(this, "winGroup", true);
        this.winGroup.scale.x = 1 / (gameObject.params.baseWidth / this.game.width);
        this.winGroup.scale.y = 1 / (gameObject.params.baseHeight / this.game.height);

        this.loseGroup = this.game.add.group(this, "loseGroup", true);
        this.loseGroup.scale.x = 1 / (gameObject.params.baseWidth / this.game.width);
        this.loseGroup.scale.y = 1 / (gameObject.params.baseHeight / this.game.height);
        /* Generate the initial Order */
        this.generateOrder();
    },
    /* Function to Scale Sprites */
    scaleSprite: function (sprite, availableSpaceWidth, availableSpaceHeight) {
        sprite.scale.x = (sprite.scale.x / (gameObject.params.baseWidth / availableSpaceWidth)) / 2;
        sprite.scale.y = (sprite.scale.y / (gameObject.params.baseHeight / availableSpaceHeight)) / 2;
    },
    /* Function to scale UI elements */
    scaleUI: function (sprite, availableSpaceWidth, availableSpaceHeight) {
        sprite.width = sprite.width / (gameObject.params.baseWidth / availableSpaceWidth);
        sprite.height = sprite.height / (gameObject.params.baseHeight / availableSpaceHeight);
    },
    /* Function to scale order additions */
    scaleAdditions: function (sprite, availableSpaceWidth, availableSpaceHeight) {
        sprite.width = (sprite.width / (gameObject.params.baseWidth / availableSpaceWidth)) / 3;
        sprite.height = (sprite.height / (gameObject.params.baseHeight / availableSpaceHeight)) / 3;
    },    
    update: function () {
        this.textScore.x = this.scorePanel.x - (this.scorePanel.width * 0.5);
        this.textScore.y = this.scorePanel.y + (this.scorePanel.height * 0.5);
        this.textClock.x = this.clock.x + (this.clock.width * 0.5);
        this.textClock.y = this.clock.y + (this.clock.height * 0.5);
    },
    /* Function to Countdown the clock */
    showTimeLeft: function () {
        if (timeLeft > 0) {
            timeLeft--;
            this.textClock.text = "" + timeLeft;
        } else {
            this.lose();
        }
    },
    /* Function to set the lose UI visible  */
    lose: function () {
        this.checkRank();
        this.clockTimer.stop();
        this.loseGroup.visible = true;
        this.buttonPlayagain = this.game.add.button(650, 630, 'playagain', this.playAgain, this);
        this.btnBack = this.game.add.button(80, 50, 'btnBack', this.backToGameStart, this);
        this.btnBack.anchor.setTo(0.5, 0.5);
        this.loseGroup.create(0, 0, 'gameover');
        this.loseGroup.create(320, 570, 'score');
        this.loseGroup.create(470, 570, 'best');
        this.scoreOnLose = this.add.text(380, 670, "", this.style);
        this.bestScore = this.add.text(540, 670, "", this.style);
        this.scoreOnLose.text = "" + this.burgerCount;
        this.bestScore.text = Persistence.load("Rank1");
        this.loseGroup.add(this.scoreOnLose);
        this.loseGroup.add(this.bestScore);
        this.loseGroup.add(this.buttonPlayagain);
        this.loseGroup.add(this.btnBack);
    },
    /* Function to add the ingredient in the order */
    addIngredient: function (ingredient) {
        if (currentRecipe.length < 6) {
            currentRecipe.push(ingredient.name);
            this.recipeGroup.create(burgerCoordinatesX, burgerCoordinatesY - ingredient.spriteHeight, ingredient.name);
            burgerCoordinatesY = burgerCoordinatesY - ingredient.spriteHeight;

        }

        this.checkWinOrLose();
    },
    addFriesSoda: function (addition) {
        if (currentRecipeAdditions.length < 2) {
            currentRecipeAdditions.push(addition.name);
            this.recipeAdditionGroup.create(additionCoordinatesX, additionCoordinatesY, addition.name);
            additionCoordinatesX = additionCoordinatesX + 400;
        }
        this.checkWinOrLose();
    },
    /* Function to generate random orders */
    generateOrder: function () {
        currentOrder[0] = "bottomBread";
        currentOrder[1] = orderIngredients[Math.floor(Math.random() * 6)];
        currentOrder[2] = "meat";
        currentOrder[3] = orderIngredients[Math.floor(Math.random() * 6)];
        currentOrder[4] = orderIngredients[Math.floor(Math.random() * 6)];
        currentOrder[5] = "topBread";
        currentOrderAdditions[0] = orderAdditions[Math.floor(Math.random() * 2)];
        currentOrderAdditions[1] = orderAdditions[Math.floor(Math.random() * 2)];
        this.showOrderAndAddition();
    },
    /* Function to show the generated order and addition */
    showOrderAndAddition: function () {
        for (let i = 0; i < currentOrder.length; i++) {
            this.orderGroup.create(150, (800) - i * 50, currentOrder[i]);
        }
        for (let i = 0; i < currentOrderAdditions.length; i++) {
            this.additionOrderGroup.create(350, (800) - i * 220, currentOrderAdditions[i]);
        }
    },
    /* Function that checks if the recipe is completed */
    checkWinOrLose: function () {
        if (currentRecipe.length == 6 && currentRecipeAdditions.length == 2 && timeLeft > 0) {
            if (this.ingredientsEqual(currentOrder, currentRecipe) && this.additionsEqual(currentOrderAdditions, currentRecipeAdditions)) {
                this.image = this.game.add.image(800, 450, 'wellDone');
                this.image.anchor.setTo(0.5, 0.5);
                this.button = this.game.add.button(800, 750, 'btnNext', this.nextOrder, this);
                this.button.anchor.setTo(0.5, 0.5);
                this.btnBack = this.game.add.button(80, 50, 'btnBack', this.backToGameStart, this);
                this.btnBack.anchor.setTo(0.5, 0.5);
                this.winGroup.add(this.image);
                this.winGroup.add(this.button);
                this.winGroup.add(this.btnBack);
                this.winGroup.visible = true;
                this.clockTimer.stop()
                this.burgerCount++;
                this.textScore.text = "Burgers:" + this.burgerCount;
            } else {
                this.lose();
            }
        }
    },
    /* Function to remove all sprite from order and recipe groups */
    nextOrder: function () {
        this.orderGroup.removeAll(true);
        this.additionOrderGroup.removeAll(true);
        this.recipeGroup.removeAll(true);
        this.recipeAdditionGroup.removeAll(true);
        this.winGroup.visible = false;
        timeLeft = 30;
        this.clockTimer = this.time.create(false);
        this.clockTimer.loop(1000, this.showTimeLeft, this);
        this.clockTimer.start();
        currentRecipe = [];
        currentRecipeAdditions = [];
        currentOrder = [];
        currentOrderAdditions = [];
        burgerCoordinatesX = 670;
        burgerCoordinatesY = 450;
        additionCoordinatesX = 450;
        additionCoordinatesY = 370;

        this.generateOrder();
    },
    /*
    Function to check if order ingredients are equals to recipe
    Parameters: arr1 -> arreglo a comparar
                arr2 -> arreglo a comparar
   */
    ingredientsEqual: function (arr1, arr2) {
        if (arr1.length !== arr2.length)
            return false;
        for (var i = arr1.length; i--;) {
            if (arr1[i] !== arr2[i]) {
                return false;
            }
        }
        return true;
    },
    /*
     Function to check if the order additions are equal to the recipe additions
     Parameters: arr1 -> arreglo a comparar
                 arr2 -> arreglo a comparar
    */
    additionsEqual: function (arr1, arr2) {
        if (arr1.length !== arr2.length)
            return false;
        for (var i = arr1.length; i--;) {
            if (arr1.sort()[i] == arr2[i]) {
                return true;
            }
            if (arr1.reverse()[i] == arr2[i]) {
                return true;
            }
        }

        return false;
    },
    /* Function to check the rank and replace it */
    checkRank: function () {
        var rankPosition;
        for (var i = 2; i >= 0; i--) {
            if (parseInt(Persistence.load("Rank" + (i + 1))) <= this.burgerCount) {
                rankPosition = i + 1;
            }
        }
        switch (rankPosition) {
            case 1:
                Persistence.save("Rank3", Persistence.load("Rank2"));
                Persistence.save("Rank2", Persistence.load("Rank1"));
                Persistence.save("Rank1", this.burgerCount);
                break;
            case 2:
                Persistence.save("Rank3", Persistence.load("Rank2"));
                Persistence.save("Rank2", this.burgerCount);
                break;
            case 3:
                Persistence.save("Rank3", this.burgerCount);
                break;
            default:
                Persistence.save("Rank" + rankPosition, this.burgerCount);
        }

    },
    showTutorial: function () {
        countTutorialStep++;
        if (parseInt(Persistence.load("tutorialFirstTime")) == 1 || Persistence.load("tutorialFirstTime") == null) {
            if (countTutorialStep == 1) {
                this.btnSkip.visible = true;
                this.bubbleOrder.visible = true;
            }
            if (countTutorialStep == 2) {
                this.bubbleOrder.visible = false;
                this.bubbleIngredients.visible = true;
            }
            if (countTutorialStep == 3) {
                this.bubbleIngredients.visible = false;
                this.bubbleClock.visible = true;
                Persistence.save("tutorialFirstTime", 0);
            }
        } else {
            this.skipTutorial();
        }
    },
    playAgain: function () {
        this.orderGroup.removeAll(true);
        this.additionOrderGroup.removeAll(true);
        this.recipeGroup.removeAll(true);
        this.recipeAdditionGroup.removeAll(true);
        this.loseGroup.visible = false;
        this.clockTimer = this.time.create(false);
        this.clockTimer.loop(1000, this.showTimeLeft, this);
        this.clockTimer.start();
        this.textScore.text = "Burgers:0"
        timeLeft = 30;
        currentRecipe = [];
        currentRecipeAdditions = [];
        currentOrder = [];
        currentOrderAdditions = [];
        burgerCoordinatesX = 670;
        burgerCoordinatesY = 450;
        additionCoordinatesX = 450;
        additionCoordinatesY = 370;
        this.burgerCount = 0;
        this.generateOrder();
    },
    skipTutorial: function () {
        this.bubbleOrder.visible = false;
        this.bubbleIngredients.visible = false;
        this.bubbleClock.visible = false;
        this.btnSkip.visible = false;
        tutorialTimer.stop();
        Persistence.save("tutorialFirstTime", 0);
        this.time.events.repeat(Phaser.Timer.SECOND, 4, this.beforeStart, this);
        countTutorialStep = 4;
    },
    beforeStart: function () {
        if (imageNumber <= 2) {
            this.timeBeforeStart.destroy();
            this.clockTimer.start();
        }
        if (imageNumber > 0) {
            this.timeBeforeStart = this.game.add.image(this.world.centerX, this.world.centerY, imageNumber);
            this.scaleUI(this.timeBeforeStart, this.game.width, this.game.height);
            this.timeBeforeStart.anchor.setTo(0.5, 0.5);
        }

        imageNumber--;
    },
    backToGameStart: function () {
        this.winGroup.visible = false;
        this.loseGroup.visible = false;
        this.orderGroup.visible = false;
        this.recipeGroup.visible = false;
        this.recipeAdditionGroup.visible = false;
        this.additionOrderGroup.visible = false;
        burgerCoordinatesX = 670;
        burgerCoordinatesY = 450;
        additionCoordinatesX = 450;
        additionCoordinatesY = 370;
        timeLeft = 30;
        currentRecipe = [];
        currentRecipeAdditions = [];
        game.state.start("GameStart");
    },
    backToGameStart: function () {
        this.winGroup.visible = false;
        this.loseGroup.visible = false;
        this.orderGroup.visible = false;
        this.recipeGroup.visible = false;
        this.recipeAdditionGroup.visible = false;
        this.additionOrderGroup.visible = false;
        burgerCoordinatesX = 670;
        burgerCoordinatesY = 450;
        additionCoordinatesX = 450;
        additionCoordinatesY = 370;
        timeLeft = 30;
        currentRecipe = [];
        currentRecipeAdditions = [];
        this.state.start("GameStart");
    }
};

function goToGamePlay() {
    this.state.start('GamePlay');
}

function goToCredits() {
    this.state.start('Credits');
}

function goToRecords() {
    this.state.start('Records');
}

function goToGameStart() {
    this.state.start('GameStart');
}


var burgerBrainiac;
window.onload = function () {
    burgerBrainiac = new Phaser.Game(gameObject.params.baseWidth, gameObject.params.baseHeight, Phaser.AUTO);
    burgerBrainiac.state.add("Boot", gameObject.Boot);
    burgerBrainiac.state.add("GameStart", gameObject.GameStart);
    burgerBrainiac.state.add("Records", gameObject.Records);
    burgerBrainiac.state.add("Credits", gameObject.Credits);
    burgerBrainiac.state.add("GamePlay", gameObject.GamePlay);
    Persistence.init();
    burgerBrainiac.state.start("Boot");
}

class Persistence {
    static save(key, value) {
        localStorage.setItem(key, value);
    }
    static load(key) {
        return localStorage.getItem(key);
    }
    static remove(key) {
        localStorage.removeItem(key);
    }
    static init() {
        if (Persistence.load("Rank1") == null) {
            localStorage.setItem("Rank1", 0);
            localStorage.setItem("Rank2", 0);
            localStorage.setItem("Rank3", 0);
            localStorage.setItem("tutorialFirstTime", 1);
        }
    }
}

